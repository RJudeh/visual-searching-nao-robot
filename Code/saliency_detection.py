from saliency_models import IttiKoch
import numpy as np
import cv2
import matplotlib.pyplot as plt
from scipy.misc import imresize
import skimage.transform

class SaliencyDetector(object):
    def __init__(self, model_params, feature_params, fns_weights, peakiness=False):

        self.saliencyModel = IttiKoch(model_params, feature_params, fns_weights, peakiness)
        self.saliencyMap = None

    def detectSaliency(self, img):

        self.saliencyMap = self.saliencyModel.predictSaliency(img)

        return self.saliencyMap

def imresize1(s_map, width):
        newheight = round(float(s_map.shape[0]) / float(s_map.shape[1]))
        newheight = int(newheight)

        newsize = [newheight * width, width]

        imsize = skimage.transform.resize(s_map, newsize, mode='reflect', preserve_range=True)

        return imsize

if __name__ == '__main__':
    """
    pic7 is not working because two of the objects are so close to each other
    pic 9 is not working
    """

    img = cv2.imread('Images/test1.jpg')
    print("img", img.shape)
    img = img[..., ::-1]
    img = imresize(img, 0.4)
    print("resized", img.shape)

    saliency_model_params = {"final_smoothing_sigma": 17, "center_bias_length": 1.5}
    saliency_feature_params = {"n_gaborangles": 4, "n_corners": 10000, "corners_smoothing_sigma": 5}
    saliency_fns_weights = {"I": 1., "O": 1., "C": 10., "Cor": 2.}

    S = SaliencyDetector(saliency_model_params, saliency_feature_params, saliency_fns_weights)
    sMap = S.detectSaliency(img)*255
    print("sssssssss", sMap.shape)
    sMap = sMap.astype(np.uint8)

    ret, otsu = cv2.threshold(sMap, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # kernel = np.ones((5, 5), np.uint8)
    # otsu = cv2.dilate(otsu, kernel, iterations=3)
    kernel = np.ones((5,5), np.uint8)
    otsu1 = cv2.erode(otsu, kernel, iterations=4)
    otsu = np.copy(otsu1)

    im2, contours, hierarchy = cv2.findContours(otsu, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    m = np.zeros_like(otsu)
    cv2.drawContours(m, contours, contourIdx=-1,color=(1,1,1), thickness=-1)
    blur = cv2.bilateralFilter(m, 60, 200, 200)
    im2, contours, hierarchy = cv2.findContours(blur, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    m2 = np.zeros_like(otsu)
    cv2.drawContours(m2, contours, contourIdx=-1, color=(1,1,1), thickness=-1)

    area = cv2.contourArea(contours[0])
    #mean_val = cv2.mean(im, mask=mask) #example of getting mean intensity of objects

    print("contours", len(contours))
    objects = []
    if contours:
        for c in contours:
            min_xy = np.squeeze(np.min(c, axis=0, keepdims=True))
            max_xy = np.squeeze(np.max(c, axis=0, keepdims=True))

            min_r = min_xy[1]
            min_c = min_xy[0]

            max_r = max_xy[1]
            max_c = max_xy[0]

            center_r = (min_r + max_r) // 2
            center_c = (min_c + max_c) // 2

            center = (center_r, center_c)
            obj = {"center": center, "boundaries": (min_r, max_r, min_c, max_c)}

            objects.append(obj)

    # print(objects)
    # p = np.zeros_like(otsu)
    # print("p",p.shape)
    # obj = objects[0]
    # min_r, max_r, min_c, max_c = obj["boundaries"]
    # center = obj["center"]
    # p[min_r:max_r, min_c:max_c] = 1
    # p[center] = 0



    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)

    ax1.imshow(img)
    ax1.axis("off")
    ax1.set_title("Original Image")

    ax2.imshow(m2, cmap="gray")
    ax2.axis("off")
    ax2.set_title("Saliency Map")

    ax3.imshow(blur, cmap="gray")
    ax3.axis("off")
    ax3.set_title("Binary Map")

    plt.show()
