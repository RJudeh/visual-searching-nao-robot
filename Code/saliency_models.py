import numpy as np
import skimage.color
import skimage.io
import skimage.transform
import skimage.filters
from scipy.stats import norm

from feature_detection import FeatureDetector

class IttiKoch(object):

    """
    Python Implementation of the Itty Koch Saliency Model
    """

    def __init__(self, model_params, feature_params, fns_weights=None, peakiness=False):

        """
        :param feature_params: a dictionary that can have the following keys:
        "phases", "n_gaborangles" for orientation map
        "ref_color" for color bias
        :param model_params: a dictionary that can have the following keys:
        "mapwidth": desired width of the saliency map
        "surround_sigmas":
        "smoothing_sigma": standard deciation of the gaussian used for smoothing the final saliency map
        "center_bias_length": the bigger the length, the narrower the center bias is
        :param fns_weights: a dictionary that has the desired features as keys whose values are the top-down weights
        :param peakiness: weighting the maps according to their peakiness (used the peakiness function)
        """
        self.featureDetector = FeatureDetector()

        self.feature_params = feature_params
        self.peakiness = peakiness
        self.fns_weights = fns_weights

        if self.fns_weights is not None:
            self.fns = list(fns_weights.keys())
        else:
            self.fns = ("I", "O", "C", "CB")

        self.imgwidth = model_params.get("imgwidth", None)
        self.mapwidth = model_params.get("mapwidth", None)
        self.surround_sigmas = model_params.get("surround_sigmas", (2, 8))
        self.final_smoothing_sigma = model_params.get("final_smoothing_sigma", None)
        self.center_bias_length = model_params.get("center_bias_length", None)

        self.saliencyMap = None
        self.saliency_maps = None

    def predictSaliency(self, img, return_maps=False):
        if self.imgwidth is not None:
            img = self.__imresize(img, self.imgwidth)

        if img.ndim == 2:
            img = img[:, :, np.newaxis]

        feature_maps = self.featureDetector.get_FeatureMaps(img, self.feature_params, self.fns)

        saliency_maps = np.stack([self._compute_saliency(feature_maps[..., m], self.surround_sigmas)
                                  for m in range(feature_maps.shape[2])], axis=-1)

        weights = self._get_weights(saliency_maps)
        weights /= weights.sum()

        self.saliencyMap = (saliency_maps*weights).sum(axis=-1)

        if self.final_smoothing_sigma is not None:
            self.saliencyMap = skimage.filters.gaussian(self.saliencyMap,
                                                        sigma=self.final_smoothing_sigma,
                                                        truncate=2, mode='reflect')

        if self.center_bias_length is not None:
            self.saliencyMap = self._apply_center_bias(self.saliencyMap, length=self.center_bias_length)

        self.saliencyMap = self.__normalize(self.saliencyMap)

        if return_maps:
            return self.saliencyMap, self.saliency_maps
        return self.saliencyMap

    def _compute_saliency(self, feature_map, surround_sig=(2, 8)):
        summap = 0
        for ssig in surround_sig:
            s_map = skimage.filters.gaussian(feature_map, sigma=ssig, truncate=2, mode="reflect")
            s_map = self.__normalize((feature_map - s_map)**2)

            wt = self.__peakiness(s_map) if self.peakiness else 1
            summap = summap + s_map * wt

        saliency_map = self.__normalize(summap)

        if self.mapwidth is not None:
            saliency_map = self.__imresize(saliency_map, self.mapwidth)

        return saliency_map

    def _get_weights(self, saliency_maps):
        if self.peakiness == True:
            weights = [self.__peakiness(saliency_maps[..., m]) for m in range(saliency_maps.shape[2])]

        elif self.fns_weights is not None:
            weights = []
            for fn in self.fns:
                if fn == "I":
                    weights = weights + [self.fns_weights[fn]]*self.featureDetector.intensity_map.shape[2]
                elif fn == "O":
                    weights = weights + [self.fns_weights[fn]]*self.featureDetector.orient_map.shape[2]
                elif fn == "C":
                    weights = weights + [self.fns_weights[fn]]*self.featureDetector.rgby_map.shape[2]
                elif fn == "CB":
                    weights = weights + [self.fns_weights[fn]]*self.featureDetector.colorbias_map.shape[2]
                elif fn == "Cor":
                    weights = weights + [self.fns_weights[fn]]*self.featureDetector.corners_map.shape[2]

        else:
            weights = [1.]*self.featureDetector.featureMaps.shape[2]

        return np.array(weights)

    def _apply_center_bias(self, s_map, length=3):
        h, w = s_map.shape[:2]

        xx = norm.pdf(np.arange(h) - h // 2, 0, h / length)
        yy = norm.pdf(np.arange(w) - w // 2, 0, w / length)

        center_bias = xx[:, np.newaxis] * yy[np.newaxis, :]
        center_bias /= center_bias.sum()
        center_bias /= center_bias.max()

        biased_map = center_bias * s_map

        return biased_map

    def __imresize(self, s_map, width):
        newheight = round(float(s_map.shape[0]) / float(s_map.shape[1]))
        newheight = int(newheight)

        newsize = [newheight * width, width]

        imsize = skimage.transform.resize(s_map, newsize, mode='reflect', preserve_range=True)

        return imsize

    def __get_local_maxima(self, data, threshold):
        refData = data[1:-1, 1:-1]

        ii, jj = np.where((refData >= data[:-2, 1:-1]) &
                          (refData >= data[2:, 1:-1]) &
                          (refData >= data[1:-1, :-2]) &
                          (refData >= data[1:-1, 2:]) &
                          (refData >= threshold))

        maxData = refData[ii, jj]

        locmax_avg = np.mean(maxData)
        locmax_sum = np.sum(maxData)
        locmax_num = len(maxData)

        return locmax_avg, locmax_num, locmax_sum

    def __peakiness(self, s_map):
        locmax_avg, locmax_num, _ = self.__get_local_maxima(s_map, 0.1)

        if locmax_num > 1:
            wt = (1 - locmax_avg) ** 2
        else:
            wt = 1

        return float(wt)

    def __normalize(self, s_map, subtract_min=True):
        if subtract_min:
            normalized_map = (s_map - s_map.min()) / (s_map.max() - s_map.min())
        else:
            normalized_map = s_map / s_map.max()

        return normalized_map
