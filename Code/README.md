# Visual Searching - NAO Robot

This project aims to allow the NAO robot to mimic visual searching in humans.

The main idea behind this project is to allow the robot to have a saliency map of the surrounding environment. Using this saliency map, the robot is able to detect
the salient objects in the environment.

The robot is then able to do one of three different tasks:
	1. The robot is able to gaze on each of the presented object in the environment by moving its head to the center of the object.
	2. One can show the robot an object of a specific colot. The robot is then able to locate the objects with the same color in a set of different objects with 
	   different colors presented in the environemnt.
	3. One can show the robot a specific object regardless of its color. The robot is then able to locate that object in a set of different objects presented in 
	   the environemnt

	

