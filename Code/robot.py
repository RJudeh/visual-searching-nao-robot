import matplotlib
matplotlib.use('QT4Agg', force=True)
from naoqi import ALProxy
from PyQt4.QtGui import QWidget, QImage, QApplication, QPainter
import sys
import almath
import numpy as np
import matplotlib.pyplot as plt
import time

from nao_camera import ImageWidget
from visual_searching_v2 import VisualSearch
from color_cue import ColorCue
from object_cue import ObjectCue

class Robot(ImageWidget):

    def __init__(self, robotIP, PORT, CameraID):

        ImageWidget.__init__(self, robotIP, PORT, CameraID)

        self.visualSearch = VisualSearch(robotIP, PORT)
        self.colorCue = ColorCue(robotIP, PORT)
        self.objectCue = ObjectCue(robotIP, PORT)

        self.postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
        self.motionProxy = ALProxy("ALMotion", robotIP, PORT)
        self.speechProxy = ALProxy("ALTextToSpeech", robotIP, PORT)
        self.lifeProxy = ALProxy("ALAutonomousLife", robotIP, PORT)
        self.events_skipped = 0

        self.postureProxy.goToPosture("Stand", 1.0)
        self.start = 0
        self.counter = 0

        global ax1, ax2, ax3
        fig, (ax1, ax2) = plt.subplots(1, 2)
        ax1.set_title("Original Image")
        ax2.set_title("Saliency Map")
        # ax3.set_title("Binary Image")

        self.yaw = 0
        self.pitch = 23.3

        self.motionProxy.setAngles("HeadYaw", self.yaw * almath.TO_RAD, 0.08)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", self.pitch * almath.TO_RAD, 0.08)  # negative: up, positive: down

    def setVelocity(self, dx, dy, ratio):
        dx = dx * ratio
        dy = dy * ratio

        self.yaw = np.clip(self.yaw + dx, -45, 45)
        self.pitch = np.clip(self.pitch + dy, -45, 45)

    def moveHead(self, headangles):
        self.motionProxy.setAngles("HeadYaw", headangles[0] * almath.TO_RAD, 0.2)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", headangles[1] * almath.TO_RAD, 0.2)  # negative: up, positive: down

    def timerEvent(self, event):
        self._updateImage()
        self.update()
        # self.speechProxy.say("I love 10")
        # self.visualSearch.searchObjects(self.npImage)
        self.colorCue.detectColoredObject(self.npImage)
        # self.objectCue.detectTargetObject(self.npImage)
        # print("state", self.objectCue.state)
        # plt.ion()
        # ax1.imshow(self.objectCue.target_obj_img, cmap="gray")
        # ax2.imshow(self.objectCue.match_img, cmap="gray")
        # # # plt.scatter(self.visualSearch.inh ibit_obj_center[1], self.visualSearch.inhibit_obj_center[0], c="r")
        # plt.show()

        # plt.ion()
        # manager = plt.get_current_fig_manager()
        # manager.window.showMaximized()
        # ax1.axis('off')
        # ax2.axis('off')
        # # ax3.axis('off')
        # ax1.imshow(self.colorCue.img)
        # ax2.imshow(self.colorCue.saliencyMap, cmap="gray")
        # # ax3.imshow(self.visualSearch.objectDetector.binary_img, cmap="gray")
        # plt.show()
        # plt.savefig("Results/img" + str(self.counter), bbox_inches='tight')
        # self.counter += 1

    def paintEvent(self, event):
        """
        Draw the QImage on screen.
        """
        painter = QPainter(self)
        #painter.drawImage(painter.viewport(), self._image)

if __name__ == '__main__':
    # IP = "10.158.125.142"  # Replace here with your NaoQi's IP address.
    IP = "169.254.100.172"  # Replace here with your NaoQi's IP address.

    PORT = 9559
    CameraID = 0

    # Read IP address from first argument if any.
    if len(sys.argv) > 1:
        IP = sys.argv[1]

    # Read CameraID from second argument if any.
    if len(sys.argv) > 2:
        CameraID = int(sys.argv[2])

    app = QApplication(sys.argv)

    NAO = Robot(IP, PORT, CameraID)

    NAO.show()
    sys.exit(app.exec_())