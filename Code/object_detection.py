import numpy as np
import cv2
import matplotlib.pyplot as plt
import skimage.transform
from scipy.misc import imresize

from saliency_detection import SaliencyDetector

class ObjectDetector(object):
    def __init__(self):

        saliency_model_params = {"final_smoothing_sigma": 10, "center_bias_length": 4}
        saliency_feature_params = {"n_gaborangles": 4}
        # saliency_fns_weights = {"I": 1., "O": 1., "C": 10.}
        saliency_fns_weights = {"C": 1.}

        self.saliencyDetector = SaliencyDetector(saliency_model_params, saliency_feature_params, saliency_fns_weights)

        self.n_objects = 0

        self.mask = None
        self.objects = None
        self.saliencyMap = None
        self.binary_img = None

    def detectObject(self, img):
        all_center = []
        all_boundaries = []
        all_saliency = []
        all_rgb = []
        all_mask = []

        if img.ndim == 3:
            self.saliencyMap = self.detectSaliency(img)
        else:
            self.saliencyMap = img

        saliency_map = np.copy(self.saliencyMap * 255)
        saliency_map = saliency_map.astype(np.uint8)

        _, binary_img = cv2.threshold(saliency_map, 0, 1, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        # _, binary_img = cv2.threshold(saliency_map, 200, 1, cv2.THRESH_BINARY)

        # binary_img = cv2.erode(binary_img, kernel=np.ones((5, 5), np.uint8), iterations=4)
        # binary_img = cv2.bilateralFilter(binary_img, 40, 200, 200)
        self.binary_img = binary_img

        _, contours, _ = cv2.findContours(binary_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        self.mask = cv2.drawContours(binary_img, contours, contourIdx=-1, color=(1, 1, 1), thickness=-1)

        if contours:
            for cnt in contours:
                min_xy = np.squeeze(np.min(cnt, axis=0, keepdims=True))
                max_xy = np.squeeze(np.max(cnt, axis=0, keepdims=True))

                min_r = min_xy[1]
                min_c = min_xy[0]
                max_r = max_xy[1]
                max_c = max_xy[0]
                obj_boundaries = (min_r, max_r, min_c, max_c)

                center_r = (min_r + max_r) // 2
                center_c = (min_c + max_c) // 2
                obj_center = (center_r, center_c)

                mask = np.zeros_like(binary_img)
                mask = cv2.drawContours(mask, [cnt, None], contourIdx=0, color=(1, 1, 1), thickness=-1)
                # mask = cv2.dilate(mask, kernel=np.ones((5, 5), np.uint8), iterations=4)

                # obj_rgb = np.round(cv2.mean(img, mask=mask)[:3])
                obj_saliency = np.round(cv2.mean(saliency_map, mask=mask)[0])
                # obj_rgb = np.round(np.mean(img[mask == 1]))
                # obj_saliency = np.round(np.mean(saliency_map[mask == 1]))

                all_center.append(obj_center)
                all_boundaries.append(obj_boundaries)
                all_saliency.append(obj_saliency)
                # all_rgb.append(obj_rgb)
                all_mask.append(mask)

            self.objects = {"centers": all_center, "boundaries": all_boundaries,
                            "saliency": np.array(all_saliency),
                            "contours": contours, "masks": all_mask}

            self.n_objects = len(all_center)

            return self.objects

        else:
            return None

    def detectSaliency(self, img):
        self.saliencyMap = self.saliencyDetector.detectSaliency(img)

        return self.saliencyMap


if __name__ == '__main__':
    img = cv2.imread('Images/test1.jpg')
    img = img[..., ::-1]
    print("img", img.shape)
    img = imresize(img, 0.4)

    print("resized", img.shape)

    objectDetector = ObjectDetector()
    objects = objectDetector.detectObject(img)
    print("objects", objectDetector.n_objects)
    #print("n_objects", len(objects))
    p = np.zeros(img.shape[:2])
    #print("contours", objects["contours"][0].shape)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(img)
    ax1.axis("off")
    ax1.set_title("Original Image")

    ax2.imshow(objectDetector.mask, cmap="gray")
    ax2.axis("off")
    ax2.set_title("Saliency Map")

    plt.show()
