import numpy as np
from naoqi import ALProxy
import almath
import time
from scipy.stats import norm
import skimage.color

from object_detection import ObjectDetector


class ColorCue(object):
    def __init__(self, robotIP, PORT, n_skip_events=2):
        self.objectDetector = ObjectDetector()

        self.memoryProxy = ALProxy("ALMemory", robotIP, PORT)
        self.speechProxy = ALProxy("ALTextToSpeech", robotIP, PORT)
        self.videoProxy = ALProxy("ALVideoDevice", robotIP, PORT)
        self.motionProxy = ALProxy("ALMotion", robotIP, PORT)

        self.memoryProxy.subscribeToEvent("TouchChanged", "ReactToTouch", "onTouched")

        self.img = None
        self.saliencyMap = None
        self.colorbias_map = None
        self.bias_color = None
        self.detected_objects = None
        self.n_objects = None
        self.skipped_events = 0
        self.n_skip_events = n_skip_events

        self.object_is_gazed = False
        self.state = 0
        self.finish = 0

        self.yaw = 0
        self.pitch = 23
        self.obj_img = None

        self.motionProxy.setAngles("HeadYaw", self.yaw * almath.TO_RAD, 0.2)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", self.pitch * almath.TO_RAD, 0.2)  # negative: up, positive: down

    def detectColoredObject(self, img):
        self.img = img

        if self.state != 2:
            self.saliencyMap = self.objectDetector.detectSaliency(self.img)
            self.detected_objects = self.objectDetector.detectObject(self.saliencyMap)

        if self.obj_img is None:
            self.obj_img = np.zeros_like(self.img)

        self.n_objects = self.objectDetector.n_objects

        front_touch = self.memoryProxy.getData("FrontTactilTouched")
        middle_touch = self.memoryProxy.getData("MiddleTactilTouched")
        rear_touch = self.memoryProxy.getData("RearTactilTouched")

        if middle_touch:
            self.state = 0
        elif front_touch:
            self.state = 1
        elif rear_touch:
            self.state = 2

        if self.state == 0:
            self.skipped_events = 0
            self._move_to_angle((0, 23))
            self.finish = 0
            return

        elif self.state == 1:

            if self.n_objects == 1:
                obj_center = self.detected_objects["centers"][0]
                print("center", obj_center)
            else:
                self.speechProxy.say("Oh! I see more than one object")
                return

            if not self.object_is_gazed:
                time.sleep(1)
                self._look_at_object(obj_center)
                self.object_is_gazed = True
                return
            else:
                if self.skipped_events < self.n_skip_events:
                    self.skipped_events += 1
                    return

                salient_obj = np.argmax(self.detected_objects["saliency"])
                mask = self.detected_objects["masks"][salient_obj]
                center = self.detected_objects["centers"][salient_obj]

                self.obj_img = self.img * mask[..., np.newaxis]

                obj_color = self.img[center[0]-10:center[0]+10, center[1]-10:center[1]+10, :]
                obj_color = np.array(obj_color)
                obj_color = np.squeeze(np.mean(obj_color, axis=(0, 1), keepdims=True))
                obj_color /= 255.

                self.bias_color = obj_color

                self._move_to_angle((0, 23))
                self.object_is_gazed = False
                self.state = 0
                return

        elif self.state == 2:
            if self.finish == 1:
                self.speechProxy.say("I believe I am looking at the right object")
                return

            if self.skipped_events < self.n_skip_events:
                self.skipped_events += 1
                return

            self.colorbias_map = self._get_colorbias_map(self.img, self.bias_color)
            self.saliencyMap = self._compute_saliency(self.colorbias_map)
            self.detected_objects = self.objectDetector.detectObject(self.saliencyMap)

            salient_obj = np.argmax(self.detected_objects["saliency"])
            print("saliency", self.detected_objects["saliency"])
            salient_obj_center = self.detected_objects["centers"][salient_obj]

            time.sleep(1)
            self._look_at_object(salient_obj_center)
            self.speechProxy.say("I believe I am looking at the right object")
            self.finish = 1
            self.state = 0
            return

    def _move_to_angle(self, angle, delay=1):
        if self.yaw != angle[0] and self.pitch != angle[1]:
            self.yaw = angle[0]
            self.pitch = angle[1]
            self._moveHead((self.yaw, self.pitch))
            time.sleep(delay)

    def _look_at_object(self, center_rc, delay=1):
        center_x, center_y = center_rc[1], center_rc[0]

        dx = np.round(center_x / float(self.img.shape[1]), 3)
        dy = np.round(center_y / float(self.img.shape[0]), 3)

        head_angles_radians = self.videoProxy.getAngularPositionFromImagePosition(0, [dx, dy])
        head_angles = np.array(head_angles_radians) * (180. / np.pi)
        head_angles = np.round(head_angles, 3)

        self._update_head_angles(head_angles)
        self._moveHead((self.yaw, self.pitch))

        time.sleep(delay)

    def _moveHead(self, head_angles):
        self.motionProxy.setAngles("HeadYaw", head_angles[0] * almath.TO_RAD, 0.2)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", head_angles[1] * almath.TO_RAD, 0.2)  # negative: up, positive: down

    def _update_head_angles(self, head_angles):
        self.yaw = self.yaw + head_angles[0]
        self.pitch = self.pitch + head_angles[1]

    def _compute_saliency(self, feature_map, surround_sig=(2, 8)):
        summap = 0
        for ssig in surround_sig:
            s_map = skimage.filters.gaussian(feature_map, sigma=ssig, truncate=2, mode="reflect")
            s_map = self.__normalize((feature_map - s_map) ** 2)

            summap = summap + s_map

        saliency_map = self.__normalize(summap)
        saliency_map = skimage.filters.gaussian(saliency_map, sigma=10, truncate=2, mode='reflect')
        saliency_map = self._apply_center_bias(saliency_map, length=4)
        saliency_map = self.__normalize(saliency_map)

        return saliency_map

    def _get_colorbias_map(self, img, bias_color):
        img_hsv = skimage.color.rgb2hsv(img)
        bias_color_hsv = skimage.color.rgb2hsv(np.resize(bias_color, (1, 1, 3)))

        hsv2xyz = lambda h, s, v: np.stack([s * np.sin(h * 2 * np.pi), s * np.cos(h * 2 * np.pi), v], axis=-1)

        ref_xyz = hsv2xyz(*bias_color_hsv.transpose((2, 0, 1)))
        img_xyz = hsv2xyz(*img_hsv.transpose((2, 0, 1)))

        colorbias_map = 1 - ((ref_xyz - img_xyz) ** 2).sum(axis=-1, keepdims=True) ** .5

        colorbias_map = np.squeeze(colorbias_map)

        return colorbias_map

    def _apply_center_bias(self, s_map, length=3):
        h, w = s_map.shape[:2]

        xx = norm.pdf(np.arange(h) - h // 2, 0, h / length)
        yy = norm.pdf(np.arange(w) - w // 2, 0, w / length)

        center_bias = xx[:, np.newaxis] * yy[np.newaxis, :]
        center_bias /= center_bias.sum()
        center_bias /= center_bias.max()

        biased_map = center_bias * s_map

        return biased_map

    def __normalize(self, s_map, subtract_min=True):
        if subtract_min:
            normalized_map = (s_map - s_map.min()) / (s_map.max() - s_map.min())
        else:
            normalized_map = s_map / s_map.max()

        return normalized_map

if __name__ == '__main__':
    IP = "169.254.100.172"  # Replace here with your NaoQi's IP address.
    PORT = 9559

    sensorProxy = ALProxy("ALSensors", IP, PORT)
    speechProxy = ALProxy("ALTextToSpeech", IP, PORT)
    memProxy = ALProxy("ALMemory", IP, PORT)
    touchProxy = ALProxy("ALTouch", IP, PORT)

    touchProxy.TouchChanged("TouchChanged")
    # memProxy.raiseEvent("FrontTactilTouched", 1)




