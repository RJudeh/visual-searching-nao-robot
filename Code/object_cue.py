import numpy as np
from naoqi import ALProxy
import almath
import time
import cv2

from object_detection import ObjectDetector


class ObjectCue(object):
    def __init__(self, robotIP, PORT, n_skip_events=4, n_matches=20):
        self.objectDetector = ObjectDetector()

        self.memoryProxy = ALProxy("ALMemory", robotIP, PORT)
        self.speechProxy = ALProxy("ALTextToSpeech", robotIP, PORT)
        self.videoProxy = ALProxy("ALVideoDevice", robotIP, PORT)
        self.motionProxy = ALProxy("ALMotion", robotIP, PORT)

        self.memoryProxy.subscribeToEvent("TouchChanged", "ReactToTouch", "onTouched")

        self.n_skip_events = n_skip_events
        self.n_matches = n_matches

        self.img = None
        self.saliencyMap = None
        self.detected_objects = None
        self.n_objects = None
        self.match_img = None
        self.target_obj_img = None

        self.object_is_gazed = False

        self.skipped_events = 0
        self.state = 0
        self.finish = 0

        self.yaw = 0
        self.pitch = 23

        self.motionProxy.setAngles("HeadYaw", self.yaw * almath.TO_RAD, 0.2)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", self.pitch * almath.TO_RAD, 0.2)  # negative: up, positive: down

    def detectTargetObject(self, img):
        self.img = img

        self.saliencyMap = self.objectDetector.detectSaliency(self.img)
        self.detected_objects = self.objectDetector.detectObject(self.saliencyMap)

        self.n_objects = self.objectDetector.n_objects

        if self.match_img is None:
            self.match_img = np.zeros_like(self.img)

        if self.target_obj_img is None:
            self.target_obj_img = np.zeros_like(self.img)

        front_touch = self.memoryProxy.getData("FrontTactilTouched")
        middle_touch = self.memoryProxy.getData("MiddleTactilTouched")
        rear_touch = self.memoryProxy.getData("RearTactilTouched")

        if middle_touch:
            self.state = 0
        elif front_touch:
            self.state = 1
        elif rear_touch:
            self.state = 2

        if self.state == 0:
            self.finish = 0
            self.skipped_events = 0
            self._move_to_angle((0, 23))
            return

        elif self.state == 1:
            if self.n_objects == 1:
                salient_obj = np.argmax(self.detected_objects["saliency"])
                obj_center = self.detected_objects["centers"][salient_obj]
                print("center", obj_center)
            else:
                self.speechProxy.say("Oh! I see more than one object")
                return

            if not self.object_is_gazed:
                time.sleep(1)
                self._look_at_object(obj_center)
                self.object_is_gazed = True
                return
            else:
                if self.skipped_events < self.n_skip_events:
                    self.skipped_events += 1
                    return

                self.target_obj_img = np.copy(self.img[50:-50, 50:-50, :])
                self.target_obj_img = np.pad(self.target_obj_img, ((50, 50), (50, 50), (0, 0)), 'constant')

                self._move_to_angle((0, 23))
                self.object_is_gazed = False
                self.state = 0
                return

        elif self.state == 2:
            if self.finish == 1:
                self.speechProxy.say("I believe I am looking at the right object")
                return

            if self.skipped_events < self.n_skip_events:
                self.skipped_events += 1
                return

            current_objects_centers = self.detected_objects["centers"]
            target_obj_idx = self._get_best_match(self.img, self.target_obj_img, current_objects_centers)
            target_obj_cntr = self.detected_objects["centers"][target_obj_idx]

            time.sleep(1)
            self._look_at_object(target_obj_cntr)
            self.speechProxy.say("I believe I am looking at the right object")
            self.finish = 1
            return

    def _get_best_match(self, img, target_obj_img, current_objects_centers):
        img_uint8 = np.copy(img)
        img_uint8 = img_uint8.astype(np.uint8)

        obj_img_uint8 = np.copy(target_obj_img)
        obj_img_uint8 = obj_img_uint8.astype(np.uint8)

        orb = cv2.ORB_create(nfeatures=5000)

        img_kp = orb.detect(img_uint8, None)
        img_kp, img_des = orb.compute(img_uint8, img_kp)
        print("img", img_kp)

        obj_kp = orb.detect(obj_img_uint8, None)
        obj_kp, obj_des = orb.compute(obj_img_uint8, obj_kp)
        print("obj", obj_kp)

        bf = cv2.BFMatcher(cv2.NORM_HAMMING)

        matches = bf.match(obj_des, img_des)
        matches = sorted(matches, key=lambda x: x.distance)

        kp_coordinates = [img_kp[match.trainIdx].pt for match in matches[:self.n_matches]]
        kp_coordinates = np.array(kp_coordinates, dtype=int)
        print("coordinate", kp_coordinates)

        target_cntr_estimate = np.array(np.mean(kp_coordinates, axis=0), dtype=int)  # center in (x, y)
        target_cntr_estimate = (target_cntr_estimate[1], target_cntr_estimate[0])  # center in (r, c)
        print("estimate", target_cntr_estimate, "actual center", self.detected_objects["centers"])

        distance = np.array([self._get_centers_distance(target_cntr_estimate, detected_cntr)
                             for detected_cntr in current_objects_centers])

        target_obj_idx = np.argmin(distance)
        # self.match_img = cv2.drawMatches(img_uint8, img_kp, obj_img_uint8,
        #                                  obj_kp, matches[:self.n_matches], outImg=None)

        return target_obj_idx

    def _get_centers_distance(self, cntr1, cntr2):
        center_distance = np.round(np.sqrt((cntr1[0] - cntr2[0]) ** 2 + (cntr1[1] - cntr2[1]) ** 2))

        return center_distance

    def _move_to_angle(self, angle, delay=1):
        if self.yaw != angle[0] and self.pitch != angle[1]:
            self.yaw = angle[0]
            self.pitch = angle[1]
            self._moveHead((self.yaw, self.pitch))
            time.sleep(delay)

    def _look_at_object(self, center_rc, delay=1):
        center_x, center_y = center_rc[1], center_rc[0]

        dx = np.round(center_x / float(self.img.shape[1]), 3)
        dy = np.round(center_y / float(self.img.shape[0]), 3)

        head_angles_radians = self.videoProxy.getAngularPositionFromImagePosition(0, [dx, dy])
        head_angles = np.array(head_angles_radians) * (180. / np.pi)
        head_angles = np.round(head_angles, 3)

        self._update_head_angles(head_angles)
        self._moveHead((self.yaw, self.pitch))

        time.sleep(delay)

    def _moveHead(self, head_angles):
        self.motionProxy.setAngles("HeadYaw", head_angles[0] * almath.TO_RAD, 0.2)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", head_angles[1] * almath.TO_RAD, 0.2)  # negative: up, positive: down

    def _update_head_angles(self, head_angles):
        self.yaw = self.yaw + head_angles[0]
        self.pitch = self.pitch + head_angles[1]

    def __normalize(self, s_map, subtract_min=True):
        if subtract_min:
            normalized_map = (s_map - s_map.min()) / (s_map.max() - s_map.min())
        else:
            normalized_map = s_map / s_map.max()

        return normalized_map

if __name__ == '__main__':
    IP = "169.254.100.172"  # Replace here with your NaoQi's IP address.
    PORT = 9559

    sensorProxy = ALProxy("ALSensors", IP, PORT)
    speechProxy = ALProxy("ALTextToSpeech", IP, PORT)
    memProxy = ALProxy("ALMemory", IP, PORT)
    touchProxy = ALProxy("ALTouch", IP, PORT)

    touchProxy.TouchChanged("TouchChanged")
    # memProxy.raiseEvent("FrontTactilTouched", 1)




