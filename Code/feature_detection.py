import numpy as np
from scipy.ndimage.filters import convolve as conv2d
import cv2
import skimage.color
import skimage.io
import skimage.transform
import skimage.filters

class FeatureDetector(object):
    def __init__(self):

        self.intensity_map = None
        self.orient_map = None
        self.rgby_map = None
        self.colorbias_map = None
        self.corners_map = None
        self.featureMaps = None

    def get_FeatureMaps(self, img, feature_params, fns=("I", "O", "C", "CB")):
        """
        :param img: RGB image
        :param feature_params: a dictionary that can have the following keys:
        "phases", "n_gaborangles" for orientation map
        "ref_color" for color bias
        :param fns: a tuple that can have the following values:
        "I" to compute intensity map
        "O" to compute orientation map
        "C" to compute RGBY map
        "CB" to compute color bias
        :return: the specified feature maps concatenated in the last dimension
        """
        funs = []
        for counter, fn in enumerate(fns):
            if fn == "I":
                funs.append(self._get_intensity_map)
            elif fn == "O":
                funs.append(self._get_orient_map)
            elif fn == "C":
                funs.append(self._get_rgby_map)
            elif fn == "CB":
                funs.append(self._get_colorbias_map)
            elif fn == "Cor":
                funs.append(self._get_corners_map)

        self.featureMaps = np.concatenate([fun(img, feature_params) for fun in funs], axis=-1)

        return self.featureMaps

    def _get_intensity_map(self, img, *args):
        self.intensity_map = np.mean(img, axis=-1, keepdims=True)

        if self.intensity_map.ndim == 2:
            self.intensity_map = self.intensity_map[:, :, np.newaxis]

        return self.intensity_map

    def _get_orient_map(self, img, feature_params):
        phases = feature_params.get("phases", (0, np.pi / 2))
        n_gaborangles = feature_params.get("n_gaborangles", 4)

        img = np.mean(img, axis=-1)

        max_angle = 180 - (180 / n_gaborangles)
        angles = np.linspace(0, max_angle, n_gaborangles)

        filters = np.stack([self.__gabor(angle, phase)
                            for angle in angles
                            for phase in phases], axis=-1)

        self.orient_map = np.stack([conv2d(img, filters[..., f], mode='reflect')
                               for f in range(filters.shape[2])], axis=-1)

        if self.orient_map.ndim == 2:
            self.orient_map = self.orient_map[:, :, np.newaxis]

        return self.orient_map

    def _get_rgby_map(self, img, *args):
        img = img/255.

        r, g, b = list(img.transpose((2, 0, 1)))
        maxrgb = np.max(img, axis=-1) + np.finfo(float).eps

        RG = (r - g) / maxrgb
        RG[maxrgb < 1/10] = 0

        BY = (b - np.min(img[..., :2], axis=-1)) / maxrgb
        BY[maxrgb < 1/10] = 0

        self.rgby_map = np.stack([RG, BY], axis=-1)

        if self.rgby_map.ndim == 2:
            self.rgby_map = self.rgby_map[:, :, np.newaxis]

        return self.rgby_map

    def _get_colorbias_map(self, img, feature_params):
        refcolor = feature_params.get("refcolor", np.array([1., 0, 0]))

        img_hsv = skimage.color.rgb2hsv(img)
        refcolor_hsv = skimage.color.rgb2hsv(np.resize(refcolor, (1, 1, 3)))

        hsv2xyz = lambda h, s, v: np.stack([s * np.sin(h * 2 * np.pi), s * np.cos(h * 2 * np.pi), v], axis=-1)

        ref_xyz = hsv2xyz(*refcolor_hsv.transpose((2, 0, 1)))
        img_xyz = hsv2xyz(*img_hsv.transpose((2, 0, 1)))

        self.colorbias_map = 1 - ((ref_xyz - img_xyz) ** 2).sum(axis=-1, keepdims=True) ** .5

        if self.colorbias_map.ndim == 2:
            self.colorbias_map = self.colorbias_map[:, :, np.newaxis]

        return self.colorbias_map

    def _get_corners_map(self, img, feature_params):
        new_img = np.copy(img)
        uint8_img = new_img.astype(np.uint8)

        n_corners = feature_params.get("n_corners", 2000)
        corners_smoothing_sigma = feature_params.get("corners_smoothing_sigma", 8)

        self.corners_map = np.zeros((uint8_img.shape[0], uint8_img.shape[1]))

        cornersDetector = cv2.ORB_create(fastThreshold=1, edgeThreshold=1, nfeatures=n_corners)
        keypoints = cornersDetector.detect(uint8_img, None)

        coordinates = [keypoints[i].pt for i in range(len(keypoints))]
        coordinates = np.array(coordinates, dtype=int)

        self.corners_map[coordinates[:, 1], coordinates[:, 0]] = 1
        self.corners_map = skimage.filters.gaussian(self.corners_map, sigma=corners_smoothing_sigma, truncate=2, mode="reflect")

        if self.corners_map.ndim == 2:
            self.corners_map = self.corners_map[:, :, np.newaxis]

        return self.corners_map

    def __gabor(self, angle, phase=0., gamma=1, stddev=10**.5, wavelen=8):

        angle = angle * (np.pi / 180)

        n_stddev = 4
        sz = int(np.ceil(n_stddev * stddev))

        x = np.linspace(-sz, sz, 2 * sz + 1)
        y = np.linspace(-sz, sz, 2 * sz + 1)
        xx, yy = np.meshgrid(x, y)

        xx_ = xx * np.cos(angle) + yy * np.sin(angle)
        yy_ = - xx * np.sin(angle) + yy * np.cos(angle)

        gaus_filter = np.exp((xx_ ** 2 + gamma ** 2 * yy_ ** 2) / (-2 * stddev ** 2))
        sin_filter = np.cos(2 * np.pi / wavelen * xx_ + phase)

        result = gaus_filter * sin_filter

        filter = result - np.mean(result)
        gabor_filter = filter / np.sqrt(np.sum(filter ** 2))

        return gabor_filter