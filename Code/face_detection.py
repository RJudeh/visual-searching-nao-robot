import numpy as np
import cv2

class FaceDetector(object):
    def __init__(self):
        self.face_cascade = cv2.CascadeClassifier('Files/haarcascade_frontalface_default.xml')
        self.eye_cascade = cv2.CascadeClassifier('Files/haarcascade_eye.xml')
        self.faceCenterX = 0
        self.faceCenterY = 0

    def detectFace(self, grayImg):
        faces = self.face_cascade.detectMultiScale(grayImg, 1.3, 5)

        for (fx,fy,fw,fh) in faces:
            ROI_gray = grayImg[fy:fy+fh, fx:fx+fw]
            eyes = self.eye_cascade.detectMultiScale(ROI_gray)

            if not len(eyes) == 2:
                continue

            eye1 = eyes[0]
            eye2 = eyes[1]

            eye1CenterX = eye1[0] + eye1[2]//2
            eye2CenterX = eye2[0] + eye2[2]//2

            eye1CenterY = eye1[1] + eye1[3]//2
            eye2CenterY = eye2[1] + eye2[3]//2

            self.faceCenterX = fx + (eye1CenterX + eye2CenterX)//2
            self.faceCenterY = fy + (eye1CenterY + eye2CenterY)//2

            #detecting only the eyes of one face (returning inside the loop)
            return faces
        return None

    def getTrack(self, img):

        grayImg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face = self.detectFace(grayImg)

        imgHeight = grayImg.shape[0]
        imgWidth = grayImg.shape[1]

        imgCenterX = imgWidth//2
        imgCenterY = imgHeight//2

        dx = float(imgCenterX - self.faceCenterX)
        dy = float(self.faceCenterY - imgCenterY)

        #normalize betweem zero and one
        dx = np.round(dx/(imgWidth//2), 3)
        dy = np.round(dy/(imgHeight//2), 3)

        return dx, dy, face

if __name__ == '__main__':

    img = cv2.imread('Images/test1.jpg')

    myFace = FaceDetector()
    dx, dy, face = myFace.getTrack(img)
    print(img.shape)
    print(myFace.faceCenterX, myFace.faceCenterY)
    print(dx, dy, face)