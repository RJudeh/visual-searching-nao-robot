import numpy as np
from naoqi import ALProxy
import skimage.color
import cv2
import almath
import time
from scipy.misc import imresize
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.stats import norm


from object_detection import ObjectDetector


class VisualSearch(object):
    def __init__(self, robotIP, PORT, inhibition_factor=1, velocity_factor=20, n_matches=10):

        self.objectDetector = ObjectDetector()

        self.speechProxy = ALProxy("ALTextToSpeech", robotIP, PORT)
        self.motionProxy = ALProxy("ALMotion", robotIP, PORT)
        self.postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)

        self.inhibition_factor = inhibition_factor
        self.velocity_factor = velocity_factor
        self.n_matches = n_matches

        self.img = None
        self.detected_objects = None
        self.saliencyMap = None

        self.visited_objects = []

        self.start = 0
        self.inhibit_obj_center = (0, 0)
        self.n_total_objects = 0
        self.count = 0
        self.n_visited_objects = 0
        self.yaw = 0
        self.state = 0
        self.pitch = 30

        self.motionProxy.setAngles("HeadYaw", self.yaw * almath.TO_RAD, 0.2)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", self.pitch * almath.TO_RAD, 0.2)  # negative: up, positive: down

    def searchObjects(self, img):
        self.img = img
        print("n_objects", self.objectDetector.n_objects)

        self.n_visited_objects = len(self.visited_objects)
        print("n_visited_objects", self.n_visited_objects)

        if self.visited_objects:
            if self.n_visited_objects == self.n_total_objects:
                self.visited_objects = []
                # self._move_to_angle((0, 40))
                self.saliencyMap = np.zeros_like(self.saliencyMap)
                self.speechProxy.say("I think I finished looking at the objects")
                # self.postureProxy.goToPosture("Sit", 1.0)

            else:
                self.saliencyMap = self.objectDetector.detectSaliency(self.img)
                self.saliencyMap = self._inhibit_visited_objects(self.saliencyMap)
                self.detected_objects = self.objectDetector.detectObject(self.saliencyMap)

        else:
            print("there are no visited objects")
            self.detected_objects = self.objectDetector.detectObject(self.img)
            self.saliencyMap = self.objectDetector.saliencyMap

            if self.start < 3:
                print("this is the first time", self.start)
                self.n_total_objects = self.objectDetector.n_objects
                self.start += 1
                # print("n_total_objects", self.n_total_objects)

        if self.detected_objects is not None and self.start >= 3:
            print("I am detecting objects")
            salient_obj = np.argmax(self.detected_objects["saliency"])
            distance_x, distance_y = self._get_obj_distance(salient_obj)
            print("xy", distance_x, distance_y)
            print("v_obj", len(self.visited_objects))

            obj_is_gazed = self._check_gazing(distance_x, distance_y)

            if obj_is_gazed:
                # self.state = 1
                print("object is gazed")
                v_mask = self.detected_objects["masks"][salient_obj]
                v_mask = v_mask[..., np.newaxis]

                v_obj_img = v_mask * img

                new_visited_object = {"obj_img": v_obj_img,
                                      "contour": self.detected_objects["contours"][salient_obj],
                                      "color": self.detected_objects["colors"][salient_obj]}

                self.visited_objects.append(new_visited_object)
                self.saliencyMap = self._inhibit_visited_objects(self.saliencyMap)
                self._move_to_angle((0, 40))
                time.sleep(0.5)


            else:
                if self.state < 3:
                    self.state += 1
                    return

                self._track_object((distance_x, distance_y))

        else:
            self._move_to_angle((0, 40))

    def _inhibit_visited_objects(self, saliencyMap, method="gaussian"):
        inhibit_map = np.zeros_like(saliencyMap)

        if method == "gaussian":
            inhibit_map = 1 - inhibit_map

        for v_obj in self.visited_objects:
            inhibit_obj = self._get_best_match(v_obj, method="opencv")

            if method == "gaussian":

                inhibit_filter = self._get_inhibition_gaussian(inhibit_obj, sigma=40)

                inhibit_map *= inhibit_filter

            elif method == "simple":
                inhibit_mat = np.zeros_like(saliencyMap)

                inhibit_mat = cv2.drawContours(inhibit_mat, self.detected_objects["contours"],
                                               contourIdx=inhibit_obj, color=(1, 1, 1), thickness=-1)

                inhibit_mat = cv2.dilate(inhibit_mat, kernel=np.ones((5, 5), np.uint8), iterations=10)
                inhibit_map += inhibit_mat * self.inhibition_factor

        if method == "simple":
            inhibit_map = 1 - inhibit_map

        inhibited_sMap = np.copy(saliencyMap)
        inhibited_sMap *= inhibit_map

        return inhibited_sMap

    def _get_inhibition_gaussian(self, inhibit_obj, sigma=2):
        h, w = self.saliencyMap.shape[:2]

        self.inhibit_obj_center = self.detected_objects["centers"][inhibit_obj]
        img_center = (self.img.shape[0] // 2, self.img.shape[1] // 2)

        r_mean = self.inhibit_obj_center[0] - img_center[0]
        c_mean =self.inhibit_obj_center[1] - img_center[1]

        r = norm.pdf(np.arange(h) - h // 2, r_mean, sigma)
        c = norm.pdf(np.arange(w) - w // 2, c_mean, sigma)

        inhibition_gaussian = r[:, np.newaxis] * c[np.newaxis, :]

        inhibition_gaussian /= inhibition_gaussian.sum()
        inhibition_gaussian /= inhibition_gaussian.max()
        inhibition_gaussian = 1. - inhibition_gaussian
        inhibition_gaussian *= 1.

        return inhibition_gaussian

    def _get_best_match(self, v_obj, method="opencv"):
        if method == "simple":
            d_cnts = self.detected_objects["contours"]
            d_clrs = self.detected_objects["colors"]

            v_cnt = v_obj["contour"]
            v_clr = v_obj["color"]

            shape_match_score = np.array([cv2.matchShapes(v_cnt, d_cnt, 1, 0.0) for d_cnt in d_cnts])
            best_shape_match = np.argmin(shape_match_score)

            color_match_score = np.array([self._get_color_match(v_clr, d_clr) for d_clr in d_clrs])
            best_color_match = np.argmax(color_match_score)

            if best_shape_match == best_color_match:
                best_match = best_shape_match

            else:
                shape_match_score = self.__normalize(shape_match_score)
                color_match_score = self.__normalize(color_match_score)

                shape_var = (shape_match_score[best_shape_match] - np.mean(shape_match_score)) ** 2
                color_var = (color_match_score[best_color_match] - np.mean(color_match_score)) ** 2

                if shape_var > color_var:
                    best_match = best_shape_match
                else:
                    best_match = best_color_match

            return best_match

        elif method == "opencv":
            best_match = self._get_features_match(v_obj)

            return best_match

    def _get_color_match(self, clr1, clr2):
        clr1_hsv = skimage.color.rgb2hsv(np.resize(clr1, (1, 1, 3)))
        clr2_hsv = skimage.color.rgb2hsv(np.resize(clr2, (1, 1, 3)))

        hsv2xyz = lambda h, s, v: np.stack([s * np.sin(h * 2 * np.pi), s * np.cos(h * 2 * np.pi), v], axis=-1)

        clr1_xyz = hsv2xyz(*clr1_hsv.transpose((2, 0, 1)))
        clr2_xyz = hsv2xyz(*clr2_hsv.transpose((2, 0, 1)))

        color_match_score = 1 - ((clr1_xyz - clr2_xyz) ** 2).sum(axis=-1, keepdims=True) ** .5

        return color_match_score

    def _get_features_match(self, v_obj):
        d_cntrs = self.detected_objects["centers"]

        v_obj_img = v_obj["obj_img"]

        img_uint8 = np.copy(self.img)
        img_uint8 = img_uint8.astype(np.uint8)

        obj_img_uint8 = np.copy(v_obj_img)
        obj_img_uint8 = obj_img_uint8.astype(np.uint8)

        orb = cv2.ORB_create(fastThreshold=1, edgeThreshold=1, nfeatures=10000)

        kp_img = orb.detect(img_uint8, None)
        kp_img, des_img = orb.compute(img_uint8, kp_img)

        kp_obj = orb.detect(obj_img_uint8, None)
        kp_obj, des_obj = orb.compute(obj_img_uint8, kp_obj)

        bf = cv2.BFMatcher(cv2.NORM_HAMMING)

        matches = bf.match(des_obj, des_img)
        matches = sorted(matches, key=lambda x: x.distance)

        obj_indxs = [kp_img[match.trainIdx].pt for match in matches[:self.n_matches]]
        obj_indxs = np.array(obj_indxs, dtype=int)

        obj_cntr = np.array(np.mean(obj_indxs, axis=0), dtype=int)
        obj_cntr = (obj_cntr[1], obj_cntr[0])

        distance = np.array([self._get_centers_distance(obj_cntr, d_cntr) for d_cntr in d_cntrs])

        best_features_match = np.argmin(distance)

        return best_features_match

    def _get_centers_distance(self, cntr1, cntr2):
        center_distance = np.round(np.sqrt((cntr1[0] - cntr2[0]) ** 2 + (cntr1[1] - cntr2[1]) ** 2))

        return center_distance

    def _track_object(self, distance):
        dx_pxl, dy_pxl = distance[0], distance[1]

        dx = np.round(dx_pxl / float((self.img.shape[1]//2)), 3)
        dy = np.round(dy_pxl / float((self.img.shape[0]//2)), 3)

        self.yaw, self.pitch = self._setVelocity(dx * self.velocity_factor, -dy * self.velocity_factor)
        self._moveHead((self.yaw, self.pitch))

    def _get_obj_distance(self, track_obj):
        img_center = (self.img.shape[0] // 2, self.img.shape[1] // 2)
        obj_center = self.detected_objects["centers"][track_obj]

        dx_pxl = np.round(float(img_center[1] - obj_center[1]))
        dy_pxl = np.round(float(img_center[0] - obj_center[0]))

        return dx_pxl, dy_pxl

    def _move_to_angle(self, angle):
        # dx = np.round(angle[0] - self.yaw, 3)
        # dy = np.round(angle[1] - self.pitch, 3)

        # self.yaw, self.pitch = self._setVelocity(dx, dy)
        self._moveHead((angle[0], angle[1]))

    def _check_gazing(self, distance_x, distance_y, accuracy=20):
        obj_is_gazed = accuracy >= distance_x >= -accuracy and\
                       accuracy >= distance_y >= -accuracy

        if obj_is_gazed:
            print("I am counting")
            self.count += 1
        else:
            self.count = 0

        if self.count == 3:
            self.count = 0
            return True
        else:
            return False

    def _moveHead(self, headangles):
        self.motionProxy.setAngles("HeadYaw", headangles[0] * almath.TO_RAD, 0.2)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", headangles[1] * almath.TO_RAD, 0.2)  # negative: up, positive: down

    def _setVelocity(self, dx, dy):
        yaw = np.clip(self.yaw + dx, -45, 45)
        pitch = np.clip(self.pitch + dy, -45, 45)

        return yaw, pitch

    def __normalize(self, x, subtract_min=True):
        if subtract_min:
            normalized_map = (x - x.min()) / (x.max() - x.min())
        else:
            normalized_map = x / x.max()

        return normalized_map


if __name__ == '__main__':
    # img1 = cv2.imread('Images/test1.jpg')
    # img1 = img1[..., ::-1]
    # img1 = imresize(img1, 0.4)
    #
    # img2 = cv2.imread('Images/test2.jpg')
    # img2 = img2[..., ::-1]
    # img2 = imresize(img2, 0.4)
    #
    # IP = "169.254.100.172"  # Replace here with your NaoQi's IP address.
    # PORT = 9559
    # V = VisualSearch(IP, PORT)
    #
    # fig, (ax1, ax2) = plt.subplots(1, 2)
    # ax1.imshow(img1)
    # ax1.axis("off")
    # ax1.set_title("Original Image")
    #
    # ax2.imshow(img2, cmap="gray")
    # ax2.axis("off")
    # ax2.set_title("Saliency Map")



    h, w = 300, 400

    inhibit_obj_center = 10, 10
    img_center = 20, 20

    r_mean = inhibit_obj_center[0] - img_center[0]
    c_mean = inhibit_obj_center[1] - img_center[1]

    r = norm.pdf(np.arange(h) - h // 2, r_mean, 1)
    c = norm.pdf(np.arange(w) - w // 2, c_mean, 1)

    inhibition_gaussian1 = r[:, np.newaxis] * c[np.newaxis, :]

    inhibition_gaussian1 /= inhibition_gaussian1.sum()
    inhibition_gaussian1 /= inhibition_gaussian1.max()
    inhibition_gaussian = 1. - inhibition_gaussian1
    inhibition_gaussian *= 1.
    print(inhibition_gaussian.shape)

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    X = np.arange(-200, 200, 1)
    Y = np.arange(-150, 150, 1)
    X, Y = np.meshgrid(X, Y)
    print(X.shape)
    print(Y.shape)

    ax.plot_surface(X, Y, inhibition_gaussian)
    plt.show()





