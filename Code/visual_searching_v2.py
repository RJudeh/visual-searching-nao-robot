import numpy as np
from naoqi import ALProxy
import almath
import time
from scipy.stats import norm

from object_detection import ObjectDetector

class VisualSearch(object):
    def __init__(self, robotIP, PORT, inhibition_factor=1, n_skip_events=4):

        self.objectDetector = ObjectDetector()

        self.speechProxy = ALProxy("ALTextToSpeech", robotIP, PORT)
        self.motionProxy = ALProxy("ALMotion", robotIP, PORT)
        self.postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
        self.videoProxy = ALProxy("ALVideoDevice", robotIP, PORT)
        self.memoryProxy = ALProxy("ALMemory", robotIP, PORT)

        self.memoryProxy.subscribeToEvent("TouchChanged", "ReactToTouch", "onTouched")

        self.inhibition_factor = inhibition_factor
        self.n_skip_events = n_skip_events

        self.img = None
        self.detected_objects = None
        self.saliencyMap = None
        self.n_total_objects = None

        self.visited_objects = []

        self.skipped_events = 0
        self.n_visited_objects = 0
        self.start = 0
        self.inhibit_obj_center = (0, 0)
        self.count = 0
        self.yaw = 0
        self.pitch = 23.3

        self.motionProxy.setAngles("HeadYaw", self.yaw * almath.TO_RAD, 0.08)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", self.pitch * almath.TO_RAD, 0.08)  # negative: up, positive: down

    def searchObjects(self, img):
        middle_touch = self.memoryProxy.getData("MiddleTactilTouched")

        if middle_touch:
            self.speechProxy.say("middle touch is pressed")
            self._move_to_angle((0, 23.3))
            self.visited_objects = []
            self.n_visited_objects = 0
            self.saliencyMap = self.objectDetector.detectSaliency(self.img)
            self.detected_objects = self.objectDetector.detectObject(self.saliencyMap)
            n_objects = self.objectDetector.n_objects
            self.n_total_objects = n_objects
            self.skipped_events = 0
            return

        self.img = img
        self.saliencyMap = self.objectDetector.detectSaliency(self.img)
        self.detected_objects = self.objectDetector.detectObject(self.saliencyMap)

        n_objects = self.objectDetector.n_objects
        print("n_objects", n_objects)

        if self.skipped_events < self.n_skip_events:
            self.skipped_events += 1
            return
        elif self.skipped_events == self.n_skip_events:
            self.skipped_events += 1
            if self.start == 0:
                self.n_total_objects = n_objects
                if self.n_total_objects == 1:
                    self.speechProxy.say("I can see only one object")
                else:
                    self.speechProxy.say("I can see now" + str(self.n_total_objects) + "objects")
                self.start = 1

        self._update_total_objects(n_objects)

        if self.n_visited_objects == self.n_total_objects:
            self.visited_objects = []
            if self.n_total_objects == 1:
                self.speechProxy.say("I think I looked at the only object")
            else:
                self.speechProxy.say("I think I looked at the" + str(self.n_total_objects) + "objects")

        if self.visited_objects:
            self.saliencyMap = self._inhibit_visited_objects(self.saliencyMap)
            self.detected_objects = self.objectDetector.detectObject(self.saliencyMap)

        print("n_total_objects", self.n_total_objects)
        print("n_visited_objects", self.n_visited_objects)

        if self.detected_objects:
            salient_obj = np.argmax(self.detected_objects["saliency"])
            obj_center = self.detected_objects["centers"][salient_obj]

            self._look_at_object(obj_center)

            new_visited_object = {"center": obj_center}
            self.visited_objects.append(new_visited_object)
            self.inhibit_obj_center = obj_center

            self.n_visited_objects = len(self.visited_objects)

            self._move_to_angle((0, 23.3))

        else:
            self.speechProxy.say("I can not see any object")

    def _update_total_objects(self, n_objects, n_counts=2):
        if self.n_total_objects != n_objects:
            self.count += 1
        else:
            self.count = 0

        if self.count == n_counts:
            self.n_total_objects = n_objects
            self._move_to_angle((0, 23.3))
            if self.n_total_objects == 1:
                self.speechProxy.say("I can see now only one object")
            else:
                self.speechProxy.say("I can see now" + str(self.n_total_objects) + "objects")
            self.visited_objects = []

    def _save_total_objects(self, n_objects):
        if self.start == 0:
            self.n_total_objects = n_objects
            self.start = 1

    def _look_at_object(self, center_rc, delay=1.3):
        center_x, center_y = center_rc[1], center_rc[0]

        dx = np.round(center_x / float(self.img.shape[1]), 3)
        dy = np.round(center_y / float(self.img.shape[0]), 3)

        head_angles_radians = self.videoProxy.getAngularPositionFromImagePosition(0, [dx, dy])
        head_angles = np.array(head_angles_radians) * (180./np.pi)
        head_angles = np.round(head_angles, 3)

        self._update_head_angles(head_angles)
        self._moveHead((self.yaw, self.pitch))

        time.sleep(delay)

    def _update_head_angles(self, head_angles):
        self.yaw = self.yaw + head_angles[0]
        self.pitch = self.pitch + head_angles[1]

    def _skip_event(self, n_skip_events):
        self.n_skip_events = n_skip_events

    def _inhibit_visited_objects(self, saliency_map):
        inhibit_map = np.ones_like(saliency_map)

        for v_obj in self.visited_objects:
            inhibit_obj_center = v_obj["center"]
            inhibit_filter = self._get_inhibition_gaussian(inhibit_obj_center, sigma=40)
            inhibit_map *= inhibit_filter

        saliency_map *= inhibit_map

        return saliency_map

    def _get_inhibition_gaussian(self, inhibit_obj_center, sigma=2):
        h, w = self.saliencyMap.shape[:2]

        img_center = (self.img.shape[0] // 2, self.img.shape[1] // 2)

        r_mean = inhibit_obj_center[0] - img_center[0]
        c_mean = inhibit_obj_center[1] - img_center[1]

        r = norm.pdf(np.arange(h) - h // 2, r_mean, sigma)
        c = norm.pdf(np.arange(w) - w // 2, c_mean, sigma)

        inhibition_gaussian = r[:, np.newaxis] * c[np.newaxis, :]

        inhibition_gaussian /= inhibition_gaussian.sum()
        inhibition_gaussian /= inhibition_gaussian.max()
        inhibition_gaussian = 1. - inhibition_gaussian
        inhibition_gaussian *= 1.

        return inhibition_gaussian

    def _move_to_angle(self, angle):
        if self.yaw != angle[0] and self.pitch != angle[1]:
            self.yaw = angle[0]
            self.pitch = angle[1]
            self._moveHead((self.yaw, self.pitch))

    def _moveHead(self, head_angles):
        self.motionProxy.setAngles("HeadYaw", head_angles[0] * almath.TO_RAD, 0.1)  # negative: right, positive: left
        self.motionProxy.setAngles("HeadPitch", head_angles[1] * almath.TO_RAD, 0.1)  # negative: up, positive: down


if __name__ == '__main__':
    IP = "169.254.100.172"  # Replace here with your NaoQi's IP address.
    PORT = 9559

    dx = np.round(100 / float((100)), 3)
    dy = np.round(100 / float((100)), 3)

    videoProxy = ALProxy("ALVideoDevice", IP, PORT)


    angles = videoProxy.getAngularPositionFromImagePosition(0, [dx, dy])
    angles = np.array(angles) * (180 / np.pi)

    print(angles)
    motionProxy = ALProxy("ALMotion", IP, PORT)
    motionProxy.setAngles("HeadYaw", 0 * almath.TO_RAD, 0.2)  # negative: right, positive: left
    motionProxy.setAngles("HeadPitch", 20 * almath.TO_RAD, 0.2)  # negative: up, positive: down

