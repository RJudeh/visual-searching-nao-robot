'''
Created on 19.01.2017

@author: Stefan
'''
from naoqi import ALProxy # import lib for Nao communication
import almath




########## NAO robot

# robot parameters
robotIP = "169.254.20.238"
PORT = 9559

# setup robot proxies
postureProxy = ALProxy("ALRobotPosture",robotIP,PORT)
motionProxy = ALProxy("ALMotion",robotIP,PORT)
 
# set initial robot posture and speech stuff
postureProxy.goToPosture("Crouch",1.0)
motionProxy.setAngles("HeadYaw", 0.0*almath.TO_RAD, 0.2)
motionProxy.setAngles("HeadPitch", 0.0*almath.TO_RAD, 0.2)
speechProxy = ALProxy("ALTextToSpeech",robotIP, PORT)
speechProxy.setLanguage("English")
speechProxy.setParameter("pitchShift", 1.0)
speechProxy.setParameter("doubleVoice", 1)
speechProxy.setParameter("doubleVoiceTimeShift", 0.3)
speechProxy.setVolume(0.3)



def perfRobotAction(headangles):
    motionProxy.setAngles("HeadYaw", headangles[0], 0.1)
    motionProxy.setAngles("HeadPitch", headangles[1], 0.1)


headangles = [0.0*almath.TO_RAD, 0.0*almath.TO_RAD]

perfRobotAction(headangles)

