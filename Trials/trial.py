import numpy as np
import matplotlib.pyplot as plt
import skimage.io
import cv2
import skimage.filters


img = cv2.imread("Images/pic3.jpg")
img = img[..., ::-1]
new_img = np.copy(img)

orb = cv2.ORB_create(fastThreshold=1, edgeThreshold=1, nfeatures=5000)
uint8_img = new_img.astype(np.uint8)
kp = orb.detect(uint8_img, None)
kp, des = orb.compute(img, kp)

FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks = 50)

flann = cv2.FlannBasedMatcher(index_params, search_params)

matches = flann.knnMatch(des1,des2,k=2)

plt.imshow(img, cmap="gray")
plt.show()
