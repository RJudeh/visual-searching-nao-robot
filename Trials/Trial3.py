from naoqi import ALProxy
from naoqi import ALBroker
import time
import sys

# IP = "169.254.100.172"  # Replace here with your NaoQi's IP address.
IP = "169.254.100.172"  # Replace here with your NaoQi's IP address.

PORT = 9559
memory = ALProxy("ALMemory", IP, PORT)

memory.subscribeToEvent("TouchChanged", "ReactToTouch", "onTouched")

while True:
    time.sleep(3)
    val = memory.getData("TouchChanged")
    print(val)
