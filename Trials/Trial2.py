
import numpy as np
from naoqi import ALProxy
from naoqi import ALModule
from naoqi import ALBroker
import almath
import time
import sys
from scipy.stats import norm
import skimage.color

class ReactToTouch(ALModule):
    """ A simple module able to react
        to touch events.
    """
    def __init__(self, name):
        ALModule.__init__(self, name)
        # No need for IP and port here because
        # we have our Python broker connected to NAOqi broker

        # Create a proxy to ALTextToSpeech for later use
        self.tts = ALProxy("ALTextToSpeech")

        # Subscribe to TouchChanged event:
        global memory
        memory = ALProxy("ALMemory")
        memory.subscribeToEvent("TouchChanged",
            "ReactToTouch",
            "onTouched")

    def onTouched(self, strVarName, value):
        """ This will be called each time a touch
        is detected.

        """
        # Unsubscribe to the event when talking,
        # to avoid repetitions
        memory.unsubscribeToEvent("TouchChanged",
            "ReactToTouch")

        self.tts.say("I am so excited")
        # Subscribe again to the event
        memory.subscribeToEvent("TouchChanged",
            "ReactToTouch", "onTouched")

if __name__ == '__main__':
    IP = "169.254.100.172"  # Replace here with your NaoQi's IP address.
    PORT = 9559
    # We need this broker to be able to construct
    # NAOqi modules and subscribe to other modules
    # The broker must stay alive until the program exists
    myBroker = ALBroker("myBroker",
       "0.0.0.0",   # listen to anyone
       0,           # find a free port and use it
       IP,          # parent broker IP
       PORT)        # parent broker port


    ReactToTouch = ReactToTouch("ReactToTouch")

    # try:
    #     while True:
    #         time.sleep(1)
    # except KeyboardInterrupt:
    #     print
    #     print "Interrupted by user, shutting down"
    #     myBroker.shutdown()
    #     sys.exit(0)

